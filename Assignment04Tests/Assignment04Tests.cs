﻿using Assignment04.Attributes;
using Assignment04.Characters;
using Assignment04.Exceptions;
using Assignment04.Items.Armor;
using Assignment04.Items.Weapon;

namespace Assignment04Tests
{
    public class Assignment04Tests
    {
        [Fact]
        public void StartingWith_InvalidPlayerName_ShouldThrow_InvalidPlayerClassException()
        {
            Player testP = new Mage()
            {
                PlayerName = null,
                PlayerClass = "Mage"
            };

            Assert.Throws<InvalidPlayerClassException>(() => Assignment04.Assignment04.InitPlayer(testP.PlayerName, testP.PlayerClass));
        }

        [Fact]
        public void StartingWith_InvalidPlayerClass_ShouldThrow_InvalidPlayerClassException()
        {
            Player testP = new Mage()
            {
                PlayerName = "TestMage",
                PlayerClass = null
            };

            Assert.Throws<InvalidPlayerClassException>(() => Assignment04.Assignment04.InitPlayer(testP.PlayerName, testP.PlayerClass));
        }

        [Fact]
        public void StartingWith_InvalidPlayerClassNotNull_ShouldThrow_InvalidPlayerClassException()
        {
            Player testP = new Mage()
            {
                PlayerName = "TestMage",
                PlayerClass = "Wizard"
            };

            Assert.Throws<InvalidPlayerClassException>(() => Assignment04.Assignment04.InitPlayer(testP.PlayerName, testP.PlayerClass));
        }

        [Fact]
        public void Mage_EquippingInvalidWeapon_ShouldThrow_InvalidWeaponException()
        {
            Player testP = new Mage()
            {
                PlayerName = "Test",
                PlayerClass = "Mage"
            };
            Weapon testW = new()
            {
                WeaponType = Assignment04.Collections.WeaponType.WEAPON_AXE,
                Stats = new WeaponAttribute()
                {
                    Damage = 100,
                    AttackSpeed = 2
                }
            };

            Assert.Throws<InvalidWeaponException>(() => Assignment04.Assignment04.EquipEqpTo(testP, testW, Assignment04.Items.Slot.SLOT_WEAPON));
        }

        [Fact]
        public void Mage_EquippingInvalidArmor_ShouldThrow_InvalidWeaponException()
        {
            Player testP = new Mage()
            {
                PlayerName = "Test",
                PlayerClass = "Mage"
            };
            Armor testA = new()
            {
                EquipmentSlot = Assignment04.Items.Slot.SLOT_BODY,
                ArmorType = Assignment04.Collections.ArmorType.ARMOR_LEATHER
            };

            Assert.Throws<InvalidArmorException>(() => Assignment04.Assignment04.EquipEqpTo(testP, testA, Assignment04.Items.Slot.SLOT_BODY));
        }

        [Fact]
        public void Ranger_EquippingInvalidWeapon_ShouldThrow_InvalidWeaponException()
        {
            Player testP = new Ranger()
            {
                PlayerName = "Test",
                PlayerClass = "Ranger"
            };
            Weapon testW = new()
            {
                WeaponType = Assignment04.Collections.WeaponType.WEAPON_AXE,
                Stats = new WeaponAttribute()
                {
                    Damage = 100,
                    AttackSpeed = 2
                }
            };

            Assert.Throws<InvalidWeaponException>(() => Assignment04.Assignment04.EquipEqpTo(testP, testW, Assignment04.Items.Slot.SLOT_WEAPON));
        }

        [Fact]
        public void Ranger_EquippingInvalidArmor_ShouldThrow_InvalidWeaponException()
        {
            Player testP = new Ranger()
            {
                PlayerName = "Test",
                PlayerClass = "Ranger"
            };
            Armor testA = new()
            {
                EquipmentSlot = Assignment04.Items.Slot.SLOT_BODY,
                ArmorType = Assignment04.Collections.ArmorType.ARMOR_PLATE
            };

            Assert.Throws<InvalidArmorException>(() => Assignment04.Assignment04.EquipEqpTo(testP, testA, Assignment04.Items.Slot.SLOT_BODY));
        }

        [Fact]
        public void Rogue_EquippingInvalidWeapon_ShouldThrow_InvalidWeaponException()
        {
            Player testP = new Rogue()
            {
                PlayerName = "Test",
                PlayerClass = "Rogue"
            };
            Weapon testW = new()
            {
                WeaponType = Assignment04.Collections.WeaponType.WEAPON_AXE,
                Stats = new WeaponAttribute()
                {
                    Damage = 100,
                    AttackSpeed = 2
                }
            };

            Assert.Throws<InvalidWeaponException>(() => Assignment04.Assignment04.EquipEqpTo(testP, testW, Assignment04.Items.Slot.SLOT_WEAPON));
        }

        [Fact]
        public void Rogue_EquippingInvalidArmor_ShouldThrow_InvalidWeaponException()
        {
            Player testP = new Rogue()
            {
                PlayerName = "Test",
                PlayerClass = "Rogue"
            };
            Armor testA = new()
            {
                EquipmentSlot = Assignment04.Items.Slot.SLOT_BODY,
                ArmorType = Assignment04.Collections.ArmorType.ARMOR_PLATE
            };

            Assert.Throws<InvalidArmorException>(() => Assignment04.Assignment04.EquipEqpTo(testP, testA, Assignment04.Items.Slot.SLOT_BODY));
        }

        [Fact]
        public void Warrior_EquippingInvalidWeapon_ShouldThrow_InvalidWeaponException()
        {
            Player testP = new Warrior()
            {
                PlayerName = "Test",
                PlayerClass = "Warrior"
            };
            Weapon testW = new()
            {
                WeaponType = Assignment04.Collections.WeaponType.WEAPON_WAND,
                Stats = new WeaponAttribute()
                {
                    Damage = 100,
                    AttackSpeed = 2
                }
            };

            Assert.Throws<InvalidWeaponException>(() => Assignment04.Assignment04.EquipEqpTo(testP, testW, Assignment04.Items.Slot.SLOT_WEAPON));
        }

        [Fact]
        public void Warrior_EquippingInvalidArmor_ShouldThrow_InvalidWeaponException()
        {
            Player testP = new Warrior()
            {
                PlayerName = "Test",
                PlayerClass = "Warrior"
            };
            Armor testA = new()
            {
                EquipmentSlot = Assignment04.Items.Slot.SLOT_BODY,
                ArmorType = Assignment04.Collections.ArmorType.ARMOR_CLOTH
            };

            Assert.Throws<InvalidArmorException>(() => Assignment04.Assignment04.EquipEqpTo(testP, testA, Assignment04.Items.Slot.SLOT_BODY));
        }

        [Fact]
        public void EquippingHighLevelWeapon_ShouldThrow_InvalidWeaponException()
        {
            Player testP = new Ranger()
            {
                PlayerName = "Test",
                PlayerClass = "Ranger"
            };
            Weapon testW = new()
            {
                EquipmentLevel = 2,
                WeaponType = Assignment04.Collections.WeaponType.WEAPON_BOW,
                Stats = new WeaponAttribute()
                {
                    Damage = 100,
                    AttackSpeed = 2
                }
            };

            Assert.Throws<InvalidWeaponException>(() => Assignment04.Assignment04.EquipEqpTo(testP, testW, Assignment04.Items.Slot.SLOT_WEAPON));
        }

        [Fact]
        public void EquippingHighLevelArmor_ShouldThrow_InvalidWeaponException()
        {
            Player testP = new Ranger()
            {
                PlayerName = "Test",
                PlayerClass = "Ranger"
            };
            Armor testA = new()
            {
                EquipmentLevel = 2,
                EquipmentSlot = Assignment04.Items.Slot.SLOT_BODY,
                ArmorType = Assignment04.Collections.ArmorType.ARMOR_MAIL
            };

            Assert.Throws<InvalidArmorException>(() => Assignment04.Assignment04.EquipEqpTo(testP, testA, Assignment04.Items.Slot.SLOT_BODY));
        }

        [Fact]
        public void Mage_EquippingValidWeapon_ShouldExecuteSuccesfully()
        {
            Player testP = new Mage()
            {
                PlayerName = "Test",
                PlayerClass = "Mage"
            };
            Weapon testW = new()
            {
                EquipmentName = "Test Wand",
                EquipmentSlot = Assignment04.Items.Slot.SLOT_WEAPON,
                EquipmentLevel = 1,
                WeaponType = Assignment04.Collections.WeaponType.WEAPON_WAND,
                Stats = new WeaponAttribute()
                {
                    Damage = 100,
                    AttackSpeed = 2
                }
            };
            Weapon testW2 = new()
            {
                EquipmentName = "Test Staff",
                EquipmentSlot = Assignment04.Items.Slot.SLOT_WEAPON,
                EquipmentLevel = 1,
                WeaponType = Assignment04.Collections.WeaponType.WEAPON_STAFF,
                Stats = new WeaponAttribute()
                {
                    Damage = 100,
                    AttackSpeed = 2
                }
            };

            string[] expected = { "Equipped Test Wand!", "Equipped Test Staff!" };
            string[] actual =
            {
                Assignment04.Assignment04.EquipEqpTo(testP, testW, Assignment04.Items.Slot.SLOT_WEAPON),
                Assignment04.Assignment04.EquipEqpTo(testP, testW2, Assignment04.Items.Slot.SLOT_WEAPON)
            };

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Ranger_EquippingValidWeapon_ShouldExecuteSuccesfully()
        {
            Player testP = new Ranger()
            {
                PlayerName = "Test",
                PlayerClass = "Ranger"
            };
            Weapon testW = new()
            {
                EquipmentName = "Test Bow",
                EquipmentSlot = Assignment04.Items.Slot.SLOT_WEAPON,
                EquipmentLevel = 1,
                WeaponType = Assignment04.Collections.WeaponType.WEAPON_BOW,
                Stats = new WeaponAttribute()
                {
                    Damage = 100,
                    AttackSpeed = 2
                }
            };

            string expected = "Equipped Test Bow!";
            string actual = Assignment04.Assignment04.EquipEqpTo(testP, testW, Assignment04.Items.Slot.SLOT_WEAPON);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Rogue_EquippingValidWeapon_ShouldExecuteSuccesfully()
        {
            Player testP = new Rogue()
            {
                PlayerName = "Test",
                PlayerClass = "Rogue"
            };
            Weapon testW = new()
            {
                EquipmentName = "Test Dagger",
                EquipmentSlot = Assignment04.Items.Slot.SLOT_WEAPON,
                EquipmentLevel = 1,
                WeaponType = Assignment04.Collections.WeaponType.WEAPON_DAGGER,
                Stats = new WeaponAttribute()
                {
                    Damage = 100,
                    AttackSpeed = 2
                }
            };
            Weapon testW2 = new()
            {
                EquipmentName = "Test Sword",
                EquipmentSlot = Assignment04.Items.Slot.SLOT_WEAPON,
                EquipmentLevel = 1,
                WeaponType = Assignment04.Collections.WeaponType.WEAPON_SWORD,
                Stats = new WeaponAttribute()
                {
                    Damage = 100,
                    AttackSpeed = 2
                }
            };

            string[] expected = { "Equipped Test Dagger!", "Equipped Test Sword!" };
            string[] actual =
            {
                Assignment04.Assignment04.EquipEqpTo(testP, testW, Assignment04.Items.Slot.SLOT_WEAPON),
                Assignment04.Assignment04.EquipEqpTo(testP, testW2, Assignment04.Items.Slot.SLOT_WEAPON)
            };

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Warrior_EquippingValidWeapon_ShouldExecuteSuccesfully()
        {
            Player testP = new Warrior()
            {
                PlayerName = "Test",
                PlayerClass = "Warrior"
            };
            Weapon testW = new()
            {
                EquipmentName = "Test Axe",
                EquipmentSlot = Assignment04.Items.Slot.SLOT_WEAPON,
                EquipmentLevel = 1,
                WeaponType = Assignment04.Collections.WeaponType.WEAPON_AXE,
                Stats = new WeaponAttribute()
                {
                    Damage = 100,
                    AttackSpeed = 2
                }
            };
            Weapon testW2 = new()
            {
                EquipmentName = "Test Hammer",
                EquipmentSlot = Assignment04.Items.Slot.SLOT_WEAPON,
                EquipmentLevel = 1,
                WeaponType = Assignment04.Collections.WeaponType.WEAPON_HAMMER,
                Stats = new WeaponAttribute()
                {
                    Damage = 100,
                    AttackSpeed = 2
                }
            };
            Weapon testW3 = new()
            {
                EquipmentName = "Test Sword",
                EquipmentSlot = Assignment04.Items.Slot.SLOT_WEAPON,
                EquipmentLevel = 1,
                WeaponType = Assignment04.Collections.WeaponType.WEAPON_SWORD,
                Stats = new WeaponAttribute()
                {
                    Damage = 100,
                    AttackSpeed = 2
                }
            };

            string[] expected = { "Equipped Test Axe!", "Equipped Test Hammer!", "Equipped Test Sword!" };
            string[] actual =
            {
                Assignment04.Assignment04.EquipEqpTo(testP, testW, Assignment04.Items.Slot.SLOT_WEAPON),
                Assignment04.Assignment04.EquipEqpTo(testP, testW2, Assignment04.Items.Slot.SLOT_WEAPON),
                Assignment04.Assignment04.EquipEqpTo(testP, testW3, Assignment04.Items.Slot.SLOT_WEAPON)
            };

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Mage_EquippingValidArmor_ShouldExecuteSuccesfully()
        {
            Player testP = new Mage()
            {
                PlayerName = "Test",
                PlayerClass = "Mage"
            };
            Armor testA = new()
            {
                EquipmentName = "Test Armor",
                EquipmentLevel = 1,
                EquipmentSlot = Assignment04.Items.Slot.SLOT_BODY,
                ArmorType = Assignment04.Collections.ArmorType.ARMOR_CLOTH,
                Stats = new PrimaryAttribute()
                {
                    Strength = 1,
                    Dexterity = 1,
                    Intellect = 1
                }
            };

            string expected = "Equipped Test Armor!";
            string actual = Assignment04.Assignment04.EquipEqpTo(testP, testA, Assignment04.Items.Slot.SLOT_BODY);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Ranger_EquippingValidArmor_ShouldExecuteSuccesfully()
        {
            Player testP = new Ranger()
            {
                PlayerName = "Test",
                PlayerClass = "Ranger"
            };
            Armor testA = new()
            {
                EquipmentName = "Test Armor",
                EquipmentLevel = 1,
                EquipmentSlot = Assignment04.Items.Slot.SLOT_BODY,
                ArmorType = Assignment04.Collections.ArmorType.ARMOR_LEATHER,
                Stats = new PrimaryAttribute()
                {
                    Strength = 1,
                    Dexterity = 1,
                    Intellect = 1
                }
            };
            Armor testA2 = new()
            {
                EquipmentName = "Test Armor2",
                EquipmentLevel = 1,
                EquipmentSlot = Assignment04.Items.Slot.SLOT_BODY,
                ArmorType = Assignment04.Collections.ArmorType.ARMOR_MAIL,
                Stats = new PrimaryAttribute()
                {
                    Strength = 1,
                    Dexterity = 1,
                    Intellect = 1
                }
            };

            string[] expected = { "Equipped Test Armor!", "Equipped Test Armor2!" };
            string[] actual =
            {
                Assignment04.Assignment04.EquipEqpTo(testP, testA, Assignment04.Items.Slot.SLOT_BODY),
                Assignment04.Assignment04.EquipEqpTo(testP, testA2, Assignment04.Items.Slot.SLOT_BODY)
            };

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Rogue_EquippingValidArmor_ShouldExecuteSuccesfully()
        {
            Player testP = new Ranger()
            {
                PlayerName = "Test",
                PlayerClass = "Rogue"
            };
            Armor testA = new()
            {
                EquipmentName = "Test Armor",
                EquipmentLevel = 1,
                EquipmentSlot = Assignment04.Items.Slot.SLOT_BODY,
                ArmorType = Assignment04.Collections.ArmorType.ARMOR_LEATHER,
                Stats = new PrimaryAttribute()
                {
                    Strength = 1,
                    Dexterity = 1,
                    Intellect = 1
                }
            };
            Armor testA2 = new()
            {
                EquipmentName = "Test Armor2",
                EquipmentLevel = 1,
                EquipmentSlot = Assignment04.Items.Slot.SLOT_BODY,
                ArmorType = Assignment04.Collections.ArmorType.ARMOR_MAIL,
                Stats = new PrimaryAttribute()
                {
                    Strength = 1,
                    Dexterity = 1,
                    Intellect = 1
                }
            };

            string[] expected = { "Equipped Test Armor!", "Equipped Test Armor2!" };
            string[] actual = 
            {
                Assignment04.Assignment04.EquipEqpTo(testP, testA, Assignment04.Items.Slot.SLOT_BODY),
                Assignment04.Assignment04.EquipEqpTo(testP, testA2, Assignment04.Items.Slot.SLOT_BODY)
            };

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Warrior_EquippingValidArmor_ShouldExecuteSuccesfully()
        {
            Player testP = new Warrior()
            {
                PlayerName = "Test",
                PlayerClass = "Warrior"
            };
            Armor testA = new()
            {
                EquipmentName = "Test Armor",
                EquipmentLevel = 1,
                EquipmentSlot = Assignment04.Items.Slot.SLOT_BODY,
                ArmorType = Assignment04.Collections.ArmorType.ARMOR_PLATE,
                Stats = new PrimaryAttribute()
                {
                    Strength = 1,
                    Dexterity = 1,
                    Intellect = 1
                }
            };

            string expected = "Equipped Test Armor!";
            string actual = Assignment04.Assignment04.EquipEqpTo(testP, testA, Assignment04.Items.Slot.SLOT_BODY);

            Assert.Equal(expected, actual);
        }
    }
}
