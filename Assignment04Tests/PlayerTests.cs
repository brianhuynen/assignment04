using Assignment04.Attributes;
using Assignment04.Characters;
using Assignment04.Items.Armor;
using Assignment04.Items.Weapon;

namespace Assignment04Tests
{
    public class PlayerTests
    {
        [Fact]
        public void Leveling_Player_ShouldIncreaseLevel()
        {
            Player testP = new Mage()
            {
                PlayerName = "TestMage",
                PlayerClass = "Mage"
            };
            testP.LevelUp();

            int expected = 2, actual = testP.PlayerLevel;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Leveling_Mage_ShouldIncreaseAttributesCorrectly()
        {
            Player testP = new Mage()
            {
                PlayerName = "TestMage",
                PlayerClass = "Mage"
            };
            testP.LevelUp();

            int[] expected = { 1 + 1, 1 + 1, 8 + 5 };
            int[] actual = { testP.TotalStats.Strength, testP.TotalStats.Dexterity, testP.TotalStats.Intellect };
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Leveling_Ranger_ShouldIncreaseAttributesCorrectly()
        {
            Player testP = new Ranger()
            {
                PlayerName = "TestRanger",
                PlayerClass = "Ranger"
            };
            testP.LevelUp();

            int[] expected = { 1 + 1, 7 + 5, 1 + 1 };
            int[] actual = { testP.TotalStats.Strength, testP.TotalStats.Dexterity, testP.TotalStats.Intellect };
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Leveling_Rogue_ShouldIncreaseAttributesCorrectly()
        {
            Player testP = new Rogue()
            {
                PlayerName = "TestRogue",
                PlayerClass = "Rogue"
            };
            testP.LevelUp();

            int[] expected = { 2 + 1, 6 + 4, 1 + 1 };
            int[] actual = { testP.TotalStats.Strength, testP.TotalStats.Dexterity, testP.TotalStats.Intellect };
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Leveling_Warrior_ShouldIncreaseAttributesCorrectly()
        {
            Player testP = new Warrior()
            {
                PlayerName = "TestWarrior",
                PlayerClass = "Warrior"
            };
            testP.LevelUp();

            int[] expected = { 5 + 3, 2 + 2, 1 + 1 };
            int[] actual = { testP.TotalStats.Strength, testP.TotalStats.Dexterity, testP.TotalStats.Intellect };
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TotalDPS_ShouldBeCalculatedProperlyPerClass()
        {
            Player[] players =
            {
                new Mage()
                {
                    PlayerName = "TestMage",
                    PlayerClass = "Mage"
                },
                new Ranger()
                {
                    PlayerName = "TestRanger",
                    PlayerClass = "Ranger"
                },
                new Rogue()
                {
                    PlayerName = "TestRogue",
                    PlayerClass = "Rogue"
                },
                new Warrior()
                {
                    PlayerName = "TestWarrior",
                    PlayerClass = "Warrior"
                }
            };

            double[] expectedDPS =
            {
                1 * (1 + (8.0/ 100)),
                1 * (1 + (7.0/ 100)),
                1 * (1 + (6.0/ 100)),
                1 * (1 + (5.0/ 100))
            };
            double[] actualDPS =
            {
                players[0].TotalDamage(),
                players[1].TotalDamage(),
                players[2].TotalDamage(),
                players[3].TotalDamage()
            };
            Assert.Equal(expectedDPS, actualDPS);
        }

        [Fact]
        public void Mage_Stats_ShouldBeAdjustedProperly_EquippingArmor()
        {
            Player testP = new Mage()
            {
                PlayerName = "TestMage",
                PlayerClass = "Mage"
            };
            Armor testA = new()
            {
                EquipmentSlot = Assignment04.Items.Slot.SLOT_BODY,
                Stats = new PrimaryAttribute()
                {
                    Strength = 1,
                    Dexterity = 2,
                    Intellect = 3
                }
            };

            testP.Equip(testA);

            int[] expected = { 2, 3, 11 };
            int[] actual = { testP.TotalStats.Strength, testP.TotalStats.Dexterity, testP.TotalStats.Intellect };

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Ranger_Stats_ShouldBeAdjustedProperly_EquippingArmor()
        {
            Player testP = new Ranger()
            {
                PlayerName = "TestRanger",
                PlayerClass = "Ranger"
            };
            Armor testA = new()
            {
                EquipmentSlot = Assignment04.Items.Slot.SLOT_BODY,
                Stats = new PrimaryAttribute()
                {
                    Strength = 1,
                    Dexterity = 2,
                    Intellect = 3
                }
            };

            testP.Equip(testA);

            int[] expected = { 2, 9, 4 };
            int[] actual = { testP.TotalStats.Strength, testP.TotalStats.Dexterity, testP.TotalStats.Intellect };

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Rogue_Stats_ShouldBeAdjustedProperly_EquippingArmor()
        {
            Player testP = new Rogue()
            {
                PlayerName = "TestRogue",
                PlayerClass = "Rogue"
            };
            Armor testA = new()
            {
                EquipmentSlot = Assignment04.Items.Slot.SLOT_BODY,
                Stats = new PrimaryAttribute()
                {
                    Strength = 1,
                    Dexterity = 2,
                    Intellect = 3
                }
            };

            testP.Equip(testA);

            int[] expected = { 3, 8, 4 };
            int[] actual = { testP.TotalStats.Strength, testP.TotalStats.Dexterity, testP.TotalStats.Intellect };

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Warrior_Stats_ShouldBeAdjustedProperly_EquippingArmor()
        {
            Player testP = new Warrior()
            {
                PlayerName = "TestWarrior",
                PlayerClass = "Warrior"
            };
            Armor testA = new()
            {
                EquipmentSlot = Assignment04.Items.Slot.SLOT_BODY,
                Stats = new PrimaryAttribute()
                {
                    Strength = 1,
                    Dexterity = 2,
                    Intellect = 3
                }
            };

            testP.Equip(testA);

            int[] expected = { 6, 4, 4 };
            int[] actual = { testP.TotalStats.Strength, testP.TotalStats.Dexterity, testP.TotalStats.Intellect };

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Mage_TotalDPS_ShouldBeAdjustedProperly_EquippingWeapons()
        {
            Player testP = new Mage()
            {
                PlayerName = "TestMage",
                PlayerClass = "Mage"
            };
            Weapon testW = new()
            {
                EquipmentSlot = Assignment04.Items.Slot.SLOT_WEAPON,
                Stats = new WeaponAttribute()
                {
                    Damage = 100,
                    AttackSpeed = 2
                }
            };

            testP.Equip(testW);

            double expected = (100 * 2) * (1 + (8.0 / 100));
            double actual = testP.TotalDamage();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Ranger_TotalDPS_ShouldBeAdjustedProperly_EquippingWeapons()
        {
            Player testP = new Ranger()
            {
                PlayerName = "TestRanger",
                PlayerClass = "Ranger"
            };
            Weapon testW = new()
            {
                EquipmentSlot = Assignment04.Items.Slot.SLOT_WEAPON,
                Stats = new WeaponAttribute()
                {
                    Damage = 100,
                    AttackSpeed = 2
                }
            };

            testP.Equip(testW);

            double expected = (100 * 2) * (1 + (7.0 / 100));
            double actual = testP.TotalDamage();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Rogue_TotalDPS_ShouldBeAdjustedProperly_EquippingWeapons()
        {
            Player testP = new Rogue()
            {
                PlayerName = "TestRogue",
                PlayerClass = "Rogue"
            };
            Weapon testW = new()
            {
                EquipmentSlot = Assignment04.Items.Slot.SLOT_WEAPON,
                Stats = new WeaponAttribute()
                {
                    Damage = 100,
                    AttackSpeed = 2
                }
            };

            testP.Equip(testW);

            double expected = (100 * 2) * (1 + (6.0 / 100));
            double actual = testP.TotalDamage();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Warrior_TotalDPS_ShouldBeAdjustedProperly_EquippingWeapons()
        {
            Player testP = new Warrior()
            {
                PlayerName = "TestWarrior",
                PlayerClass = "Warrior"
            };
            Weapon testW = new()
            {
                EquipmentSlot = Assignment04.Items.Slot.SLOT_WEAPON,
                Stats = new WeaponAttribute()
                {
                    Damage = 100,
                    AttackSpeed = 2
                }
            };

            testP.Equip(testW);

            double expected = (100 * 2) * (1 + (5.0 / 100));
            double actual = testP.TotalDamage();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Mage_TotalDPS_ShouldBeAdjustedProperly_EquippingWeaponAndArmor()
        {
            Player testP = new Mage()
            {
                PlayerName = "TestMage",
                PlayerClass = "Mage"
            };
            Armor testA = new()
            {
                EquipmentSlot = Assignment04.Items.Slot.SLOT_BODY,
                Stats = new PrimaryAttribute()
                {
                    Strength = 1,
                    Dexterity = 2,
                    Intellect = 3
                }
            };
            Weapon testW = new()
            {
                EquipmentSlot = Assignment04.Items.Slot.SLOT_WEAPON,
                Stats = new WeaponAttribute()
                {
                    Damage = 100,
                    AttackSpeed = 2
                }
            };

            testP.Equip(testA);
            testP.Equip(testW);

            double expected = (100 * 2) * (1 + ((8.0 + 3.0) / 100));
            double actual = testP.TotalDamage();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Ranger_TotalDPS_ShouldBeAdjustedProperly_EquippingWeaponAndArmor()
        {
            Player testP = new Ranger()
            {
                PlayerName = "TestRanger",
                PlayerClass = "Ranger"
            };
            Armor testA = new()
            {
                EquipmentSlot = Assignment04.Items.Slot.SLOT_BODY,
                Stats = new PrimaryAttribute()
                {
                    Strength = 1,
                    Dexterity = 2,
                    Intellect = 3
                }
            };
            Weapon testW = new()
            {
                EquipmentSlot = Assignment04.Items.Slot.SLOT_WEAPON,
                Stats = new WeaponAttribute()
                {
                    Damage = 100,
                    AttackSpeed = 2
                }
            };

            testP.Equip(testA);
            testP.Equip(testW);

            double expected = (100 * 2) * (1 + ((7.0 + 2.0) / 100));
            double actual = testP.TotalDamage();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Rogue_TotalDPS_ShouldBeAdjustedProperly_EquippingWeaponAndArmor()
        {
            Player testP = new Rogue()
            {
                PlayerName = "TestRogue",
                PlayerClass = "Rogue"
            };
            Armor testA = new()
            {
                EquipmentSlot = Assignment04.Items.Slot.SLOT_BODY,
                Stats = new PrimaryAttribute()
                {
                    Strength = 1,
                    Dexterity = 2,
                    Intellect = 3
                }
            };
            Weapon testW = new()
            {
                EquipmentSlot = Assignment04.Items.Slot.SLOT_WEAPON,
                Stats = new WeaponAttribute()
                {
                    Damage = 100,
                    AttackSpeed = 2
                }
            };

            testP.Equip(testA);
            testP.Equip(testW);

            double expected = (100 * 2) * (1 + ((6.0 + 2.0) / 100));
            double actual = testP.TotalDamage();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Warrior_TotalDPS_ShouldBeAdjustedProperly_EquippingWeaponAndArmor()
        {
            Player testP = new Warrior()
            {
                PlayerName = "TestWarrior",
                PlayerClass = "Warrior"
            };
            Armor testA = new()
            {
                EquipmentSlot = Assignment04.Items.Slot.SLOT_BODY,
                Stats = new PrimaryAttribute()
                {
                    Strength = 1,
                    Dexterity = 2,
                    Intellect = 3
                }
            };
            Weapon testW = new()
            {
                EquipmentSlot = Assignment04.Items.Slot.SLOT_WEAPON,
                Stats = new WeaponAttribute()
                {
                    Damage = 100,
                    AttackSpeed = 2
                }
            };

            testP.Equip(testA);
            testP.Equip(testW);

            double expected = (100 * 2) * (1 + ((5.0 + 1.0) / 100));
            double actual = testP.TotalDamage();

            Assert.Equal(expected, actual);
        }
    }
}