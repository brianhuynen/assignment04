# Assignment04 - Console-based RPG Character Creator

A console-based application where you can create an RPG character by inputting a name and specifying a player class. This application allows the user to level up, equip equipment and view their stats and equipment through console input.

### Usage:

Upon opening "Assignment04.sln", the user is prompted to give a player name. This name has to be of length 2-20 characters and cannot contain any spaces or symbols.
From there on, any choice the user makes is specified through filling in a number. Any input not given by the console will not be accepted and the user is prompted to give a valid input.

