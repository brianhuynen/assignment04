﻿namespace Assignment04.Items
{
    /// <summary>
    /// Enum that specifies all equipment slots as type Slot.
    /// </summary>
    public enum Slot
    {
        SLOT_HEAD,
        SLOT_BODY,
        SLOT_LEGS,
        SLOT_WEAPON
    }
}
