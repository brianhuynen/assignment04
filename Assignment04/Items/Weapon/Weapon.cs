﻿using Assignment04.Attributes;
using Assignment04.Collections;

namespace Assignment04.Items.Weapon
{
    /// <summary>
    /// Weapon class which inherits the fields specified in Equipment.
    /// </summary>
    public class Weapon : Equipment
    {
        /// <summary>
        /// The type of weapon represented in the enum WeaponType.
        /// </summary>
        public WeaponType WeaponType { get; set; }
        /// <summary>
        /// The attributes of the weapon. Stores Damage and Attack Speed.
        /// </summary>
        public WeaponAttribute Stats { get; set; }
        public Weapon() { }
        /// <summary>
        /// Prints all information about the weapon.
        /// </summary>
        /// <returns>A string representative of the weapon data: Name, min. level, weapon Slot, weapon type and stats.</returns>
        public override string EquipmentData()
        {
            return 
                $"\n" +
                $"Name: {this.EquipmentName}\n" +
                $"Lvl : {this.EquipmentLevel}\n" +
                $"Slot: {this.EquipmentSlot}\n" +
                $"Type: {this.WeaponType}\n" +
                $"Stats\n" +
                $"Dmg : {this.Stats.Damage}\n" +
                $"ASPD: {this.Stats.AttackSpeed}";
        }
        /// <summary>
        /// Prints the stats of the weapon.
        /// </summary>
        /// <returns>A string representative of the weapon stats: Damage and Attack Speed.</returns>
        public override string EquipmentStats()
        {
            return
                $"Damage    : {this.Stats.Damage}\n" +
                $"AttackSPD : {this.Stats.AttackSpeed}\n";
        }
        /// <summary>
        /// Calculates the DPS of this weapon.
        /// </summary>
        /// <returns>A double representing the DPS.</returns>
        public double DPS()
        {
            return this.Stats.Damage * this.Stats.AttackSpeed;
        }
    }
}
