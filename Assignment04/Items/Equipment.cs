﻿using Assignment04.Attributes;

namespace Assignment04.Items
{
    /// <summary>
    /// Abstract equipment class which is inherited by the subclasses Armor and Weapon.
    /// </summary>
    public abstract class Equipment
    {
        /// <summary>
        /// The name of the equipment.
        /// </summary>
        public string EquipmentName { get; set; }
        /// <summary>
        /// The minimum level required to equip this equipment.
        /// </summary>
        public int EquipmentLevel { get; set; }
        /// <summary>
        /// The equipment slot this equipment occupies.
        /// </summary>
        public Slot EquipmentSlot { get; set; }

        public Equipment() { }

        public abstract string EquipmentStats();

        public abstract string EquipmentData();
    }
}
