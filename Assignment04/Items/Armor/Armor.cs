﻿using Assignment04.Attributes;
using Assignment04.Collections;

namespace Assignment04.Items.Armor
{
    /// <summary>
    /// Armor class which inherits the fields specified in Equipment.
    /// </summary>
    public class Armor : Equipment
    {
        /// <summary>
        /// The type of armor represented in the enum ArmorType.
        /// </summary>
        public ArmorType ArmorType { get; set; }
        /// <summary>
        /// The attributes of the armor. Stores Strength, Dexterity and Intellect.
        /// </summary>
        public PrimaryAttribute Stats { get; set; }
        public Armor() { }
        /// <summary>
        /// Prints all information about the armor.
        /// </summary>
        /// <returns>A string representative of the armor data: Name, min. level, armor slot, armor type and stats.</returns>
        public override string EquipmentData()
        {
            return
                $"\n" +
                $"Name: {this.EquipmentName}\n" +
                $"Lvl : {this.EquipmentLevel}\n" +
                $"Slot: {this.EquipmentSlot}\n" +
                $"Type: {this.ArmorType}\n" +
                $"Stats\n" +
                $"Str : {this.Stats.Strength}\n" +
                $"Dex : {this.Stats.Dexterity}\n" +
                $"Int : {this.Stats.Intellect}";
        }
        /// <summary>
        /// Prints the stats of the armor.
        /// </summary>
        /// <returns>A string representative of the armor stats: Strength, Dexterity and Intellect.</returns>
        public override string EquipmentStats()
        {
            return
                $"Str : {this.Stats.Strength}\n" +
                $"Dex : {this.Stats.Dexterity}\n" +
                $"Int : {this.Stats.Intellect}\n";
        }
    }
}
