﻿using System.Text.RegularExpressions;
using Assignment04.Exceptions;
using Assignment04.Characters;
using Assignment04.Collections;
using Assignment04.Items;
using Assignment04.Items.Weapon;
using Assignment04.Items.Armor;

namespace Assignment04
{
    public class Assignment04
    {
        // Instantiate the Lists of items in Items.cs
        public static ItemCollections items = new();
        // Regular expression to ensure a string only contains alphanumeric characters
        public static Regex nameFormat = new Regex("^[a-zA-Z0-9]*$");
        /// <summary>
        /// Due to the way the equipment and player instantiation logic is implemented,
        /// This constructor is meant for testing purposes only.
        /// </summary>
        public Assignment04() { }
        public static void Main(string[] args)
        {
            string[] classes = {"Mage", "Ranger", "Rogue", "Warrior"};
            string userName = "", className = "";
            // The loop keeps going until a valid username has been input.
            while (userName == "")
            {
                Console.WriteLine("Enter username (Alphanumeric, no symbols, min. length 2, max. length 20):");
                userName = Console.ReadLine();
                // In case the userName somehow ends up as null.
                if (userName == null)
                {
                    Console.WriteLine("Invalid input");
                    userName = ""; // As long the username is invalid, it stays an empty string.
                }
                // If the string does not only contain alphanumeric characters, it must contain a symbol.
                else if (!nameFormat.IsMatch(userName))
                {
                    Console.WriteLine("Your name cannot contain any symbols.");
                    userName = "";
                }
                // Handles playernames being too short.
                else if (userName.Length < 2)
                {
                    Console.WriteLine("Your name is too short.");
                    userName = "";
                }
                // Handles playernames being too short.
                else if (userName.Length > 20)
                {
                    Console.WriteLine("Your name is too long.");
                    userName = "";
                }
            }

            Console.Clear();
            // The loop keeps going until a valid classname has been input.
            while (className == "")
            {
                Console.WriteLine($"Please specify your class, {userName} (1 = Mage, 2 = Ranger, 3 = Rogue, 4 = Warrior):");
                className = Console.ReadLine();
                // Checks whether the input is an integer and returns that integer value as classId.
                bool valid = Int32.TryParse(className, out int classId);
                
                if (!valid || classId <= 0 || classId > 4)
                {
                    Console.WriteLine("Invalid Input.");
                    className = ""; // As long the classname is invalid, className remains an empty string.
                }
                else
                { 
                    className = classes[classId - 1];
                }
            }

            Console.WriteLine($"{userName} the {className}, your adventure is about to begin...");
            Player player = InitPlayer(userName, className);
            Thread.Sleep(1000);
            Console.Clear();
            //Starts the application
            Start(player);
        }
        /// <summary>
        /// Initializes the player.
        /// </summary>
        /// <param name="userName">The name of the Player</param>
        /// <param name="className">The class of the Player</param>
        /// <returns>Throws an exception if anything goes wrong initializing the player. </returns>
        /// <exception cref="InvalidPlayerClassException"></exception>
        public static Player InitPlayer(string userName, string className)
        {
            if(userName == null || className == null)
                throw new InvalidPlayerClassException("Username is null or Player is trying to instantiate a class that is null");

            switch (className)
            {
                case "Mage":
                    return new Mage() { PlayerName = userName, PlayerClass = className };
                case "Ranger":
                    return new Ranger() { PlayerName = userName, PlayerClass = className };
                case "Rogue":
                    return new Rogue() { PlayerName = userName, PlayerClass = className };
                case "Warrior":
                    return new Warrior() { PlayerName = userName, PlayerClass = className };
                default:
                    // If anything goes wrong, throw an exception
                    throw new InvalidPlayerClassException("Player class does not exist");
            }
        }
        /// <summary>
        /// Handles starting the console application.
        /// </summary>
        /// <param name="player">The Player on which the operations are performed.</param>
        public static void Start(Player player)
        {
            bool isRunning = true;
            //Keeps running until the exit input is given or an error is thrown.
            while(isRunning)
            {
                Console.WriteLine($"Player: {player.PlayerName} the {player.PlayerClass}");
                Console.WriteLine("Please input an action (1 = level up, 2 = view stats, 3 = equip item, 4 = view equipment, 5 = close): ");
                string input = Console.ReadLine();
                bool valid = Int32.TryParse(input, out int choice);

                if (valid)
                {
                    switch (choice)
                    {
                        case 1: // Levels the player.
                            Console.Clear();
                            Console.WriteLine(player.LevelUp() + "\n");
                            break;
                        case 2: // Views the player data.
                            Console.Clear();
                            Console.WriteLine(player.ViewPlayer() + "\n");
                            break;
                        case 3: // Equipping equipment
                            Console.Clear();
                            try
                            {
                                Console.WriteLine(HandleEquip(player));
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.GetType() + ": " + e.Message);
                            }
                            break;
                        case 4: // Views the equipped equipment.
                            Console.Clear();
                            Console.WriteLine(player.ViewEquipment() + "\n");
                            break;
                        case 5: // Terminates the application.
                            Console.WriteLine($"Goodbye, {player.PlayerName} the {player.PlayerClass}.");
                            isRunning = false;
                            break;
                        default: // In case an invalid input is given.
                            Console.WriteLine($"\"{choice}\" is not a valid command.");
                            break;
                    }
                }
            }
        }
        /// <summary>
        /// Prompts the player to specify an equipment and handles equipping this to the player.
        /// </summary>
        /// <param name="player">The player</param>
        /// <returns>Returns a string containing the error type and message if something went wrong.</returns>
        /// <exception cref="InvalidArmorException"></exception>
        /// <exception cref="InvalidWeaponException"></exception>
        public static string HandleEquip(Player player)
        {
            bool isEquipping = true;
            Equipment chosenEqp = null;
            // Keeps running until the equipping is handled or an error is thrown.
            while (isEquipping)
            {
                Console.WriteLine($"Player: {player.PlayerName} the {player.PlayerClass}");
                Console.WriteLine("Please select an equipment type (1 = Armor, 2 = Weapon):");
                string input = Console.ReadLine();
                bool valid = Int32.TryParse(input, out int choice);

                if (valid)
                {
                    switch (choice)
                    {
                        case 1: //In case Armor is picked, prompt armor choice
                            chosenEqp = PickArmor();
                            break;
                        case 2: //In case Weapon is picked, prompt weapon choice
                            chosenEqp = PickWeapon();
                            break;
                        default:
                            Console.WriteLine("Invalid number.");
                            break;
                    }
                }
                // Ensures we are not trying to equip equipment that is null.
                if (chosenEqp != null)
                {
                    Console.WriteLine($"Player: {player.PlayerName} the {player.PlayerClass}");
                    Console.WriteLine($"Please select a slot to equip {chosenEqp.EquipmentName} to (1 = Head, 2 = Body, 3 = Legs, 4 = Weapon):");
                    input = Console.ReadLine();
                    valid = Int32.TryParse(input, out int slotID);
                    // Ensures that the chosen equipment is equipped to the correct slot. It will throw an exception if an invalid equipment is being equipped.
                    if (valid)
                    {
                        switch (slotID)
                        {
                            case 1: // Only head armor should be equipped to the head slot.
                                if (choice == 1 && chosenEqp.EquipmentSlot == Slot.SLOT_HEAD)
                                    EquipEqpTo(player, chosenEqp, Slot.SLOT_HEAD);
                                else
                                    throw new InvalidArmorException($"Slot type mismatch ({chosenEqp.EquipmentSlot} != {Slot.SLOT_HEAD}).");
                                isEquipping = false;
                                break;
                            case 2: // Only body armor should be equipped to the body slot.
                                if (choice == 1 && chosenEqp.EquipmentSlot == Slot.SLOT_BODY)
                                    EquipEqpTo(player, chosenEqp, Slot.SLOT_BODY);
                                else
                                    throw new InvalidArmorException($"Slot type mismatch ({chosenEqp.EquipmentSlot} != {Slot.SLOT_BODY}).");
                                isEquipping = false;
                                break;
                            case 3: // Only leg armor should be equipped to the legs slot.
                                if (choice == 1 && chosenEqp.EquipmentSlot == Slot.SLOT_LEGS)
                                    EquipEqpTo(player, chosenEqp, Slot.SLOT_LEGS);
                                else
                                    throw new InvalidArmorException($"Slot type mismatch ({chosenEqp.EquipmentSlot} != {Slot.SLOT_LEGS}).");
                                isEquipping = false;
                                break;
                            case 4: // Only weapons should be equipped to the weapon slot.
                                if (choice == 2 && chosenEqp.EquipmentSlot == Slot.SLOT_WEAPON)
                                    EquipEqpTo(player, chosenEqp, Slot.SLOT_WEAPON);
                                else
                                    throw new InvalidArmorException($"Slot type mismatch ({chosenEqp.EquipmentSlot} != {Slot.SLOT_WEAPON}).");
                                isEquipping = false;
                                break;
                            default: // If the input is invalid.
                                Console.WriteLine("Invalid number.");
                                break;
                        }
                    }
                }
            }
            return "Equipping terminated succesfully.";
        }
        /// <summary>
        /// Prompts the player to pick an armor.
        /// </summary>
        /// <returns>Throws an exception if an invalid armor is picked.</returns>
        /// <exception cref="InvalidArmorException"></exception>
        public static Armor PickArmor()
        {
            bool isPicking = true;

            while(isPicking)
            {
                Console.WriteLine("Please pick type of armor (1 = Head, 2 = Body, 3 = Legs):");
                string input = Console.ReadLine();
                bool valid = Int32.TryParse(input, out int choice);

                if (valid)
                {
                    int i, armID;

                    switch (choice)
                    {
                        case 1: // Picking head armor.
                            do
                            {
                                i = 1;
                                // Displays the available armor and its index.
                                Console.WriteLine("Pick your Armor (enter number):");
                                foreach (Armor a in items.headArmor)
                                    Console.WriteLine(i++ + " - " + a.EquipmentName);

                                input = Console.ReadLine();
                                valid = Int32.TryParse(input, out armID);
                                // Ensures the armor exists and the input is not negative.
                                if (!(valid && armID > 0 && armID <= items.legArmor.Count))
                                    Console.WriteLine("Please enter a valid number:");
                            }
                            while (!(valid && armID > 0 && armID <= items.headArmor.Count));

                            return items.headArmor[armID - 1];
                        case 2: // Picking body armor.
                            do
                            {
                                i = 1;
                                // Displays the available armor and its index.
                                Console.WriteLine("Pick your Armor (enter number):");
                                foreach (Armor a in items.bodyArmor)
                                    Console.WriteLine(i++ + " - " + a.EquipmentName);

                                input = Console.ReadLine();
                                valid = Int32.TryParse(input, out armID);
                                // Ensures the armor exists and the input is not negative.
                                if (!(valid && armID > 0 && armID <= items.legArmor.Count))
                                    Console.WriteLine("Please enter a valid number:");
                            }
                            while (!(valid && armID > 0 && armID <= items.bodyArmor.Count));

                            return items.bodyArmor[armID - 1];
                        case 3: // Picking leg armor.
                            do
                            {
                                i = 1;
                                // Displays the available armor and its index.
                                Console.WriteLine("Pick your Armor (enter number):");
                                foreach (Armor a in items.legArmor)
                                    Console.WriteLine(i++ + " - " + a.EquipmentName);

                                input = Console.ReadLine();
                                valid = Int32.TryParse(input, out armID);
                                // Ensures the armor exists and the input is not negative.
                                if(!(valid && armID > 0 && armID <= items.legArmor.Count))
                                    Console.WriteLine("Please enter a valid number:");
                            }
                            while (!(valid && armID > 0 && armID <= items.legArmor.Count));

                            return items.legArmor[armID - 1];
                        default: // If invalid input has been given.
                            Console.WriteLine("Invalid number.");
                            break;
                    }
                }
            }
            // Throw an exception if picking armor terminated unexpectedly
            throw new InvalidArmorException("Something went wrong picking armor");
        }
        /// <summary>
        /// Prompts the player to pick a weapon.
        /// </summary>
        /// <returns>Throws an exception if an invalid weapon has been picked.</returns>
        /// <exception cref="InvalidWeaponException"></exception>
        public static Weapon PickWeapon()
        {
            bool isPicking = true;
            while(isPicking)
            {
                int i = 1;
                // Displays the available weapons and its index.
                Console.WriteLine("Please pick a weapon (enter number):");
                foreach (Weapon w in items.weapons)
                    Console.WriteLine(i++ + " - " + w.EquipmentName);

                string input = Console.ReadLine();
                bool valid = Int32.TryParse(input, out int weapID);

                // Ensures the weapon exists and the input is not negative.
                if (valid && weapID > 0 && weapID <= items.weapons.Count)
                    return items.weapons[weapID - 1];
                else
                    Console.WriteLine("Please enter a valid number:");
            }
            // Throw an exception if picking weapons terminated unexpectedly
            throw new InvalidWeaponException("Something went wrong picking a weapon");
        }
        /// <summary>
        /// Handles equipping an equipment to a specified slot for a player.
        /// </summary>
        /// <param name="player">The player.</param>
        /// <param name="eqp">The equipment to be equipped.</param>
        /// <param name="slot">The equipment slot to which the equipment will be equipped.</param>
        /// <exception cref="InvalidArmorException">When invalid armor is being equipped</exception>
        /// <exception cref="InvalidWeaponException">When invalid weapons are being equipped</exception>
        /// <exception cref="InvalidPlayerClassException">If the player is of an invalid class</exception>
        public static string EquipEqpTo(Player player, Equipment eqp, Slot slot)
        {
            string msg = player.CanEquip(eqp);
            if (msg == "")
            {
                try // Try equipping the equipment to the player
                {
                    player.Equip(eqp);
                }
                catch // If there exists an equipment on the specified slot, remove the equipment from that slot first, then equip again.
                {
                    player.Unequip(slot);
                    player.Equip(eqp);
                }
                return $"Equipped {eqp.EquipmentName}!";
            }
            else if (eqp.GetType() == typeof(Armor))
                throw new InvalidArmorException($"Class {player.PlayerClass} cannot equip this armor: {msg}");
            else
                throw new InvalidWeaponException($"Class {player.PlayerClass} cannot equip this weapon: {msg}");
        }
    }
}