﻿using Assignment04.Items;
using Assignment04.Items.Armor;
using Assignment04.Items.Weapon;

namespace Assignment04.Collections
{
    /// <summary>
    /// Class that stores all data of available Equipment of type Armor and Weapon.
    /// </summary>
    public class ItemCollections
    {
        /// <summary>
        /// Stores all available Armor occupying the head slot.
        /// </summary>
        public List<Armor> headArmor = new()
        {
            new Armor()
            {
                EquipmentName = "Simple Wizard Hat",
                EquipmentLevel = 1,
                EquipmentSlot = Slot.SLOT_HEAD,
                ArmorType = ArmorType.ARMOR_CLOTH,
                Stats = new()
                {
                    Strength = 1,
                    Dexterity = 1,
                    Intellect = 4,
                }
            },
            new Armor()
            {
                EquipmentName = "Advanced Wizard Hat",
                EquipmentLevel = 5,
                EquipmentSlot = Slot.SLOT_HEAD,
                ArmorType = ArmorType.ARMOR_CLOTH,
                Stats = new()
                {
                    Strength = 10,
                    Dexterity = 10,
                    Intellect = 40,
                }
            },
            new Armor()
            {
                EquipmentName = "Simple Leather Cap",
                EquipmentLevel = 1,
                EquipmentSlot = Slot.SLOT_HEAD,
                ArmorType = ArmorType.ARMOR_LEATHER,
                Stats = new()
                {
                    Strength = 1,
                    Dexterity = 4,
                    Intellect = 1,
                }
            },
            new Armor()
            {
                EquipmentName = "Advanced Leather Cap",
                EquipmentLevel = 5,
                EquipmentSlot = Slot.SLOT_HEAD,
                ArmorType = ArmorType.ARMOR_LEATHER,
                Stats = new()
                {
                    Strength = 10,
                    Dexterity = 40,
                    Intellect = 10,
                }
            },
            new Armor()
            {
                EquipmentName = "Simple Mail Helmet",
                EquipmentLevel = 1,
                EquipmentSlot = Slot.SLOT_HEAD,
                ArmorType = ArmorType.ARMOR_MAIL,
                Stats = new()
                {
                    Strength = 2,
                    Dexterity = 4,
                    Intellect = 1,
                }
            },
            new Armor()
            {
                EquipmentName = "Advanced Mail Helmet",
                EquipmentLevel = 5,
                EquipmentSlot = Slot.SLOT_HEAD,
                ArmorType = ArmorType.ARMOR_MAIL,
                Stats = new()
                {
                    Strength = 20,
                    Dexterity = 40,
                    Intellect = 10,
                }
            },
            new Armor()
            {
                EquipmentName = "Simple Plate Helmet",
                EquipmentLevel = 1,
                EquipmentSlot = Slot.SLOT_HEAD,
                ArmorType = ArmorType.ARMOR_PLATE,
                Stats = new()
                {
                    Strength = 4,
                    Dexterity = 1,
                    Intellect = 1,
                }
            },
            new Armor()
            {
                EquipmentName = "Advanced Plate Helmet",
                EquipmentLevel = 5,
                EquipmentSlot = Slot.SLOT_HEAD,
                ArmorType = ArmorType.ARMOR_PLATE,
                Stats = new()
                {
                    Strength = 40,
                    Dexterity = 10,
                    Intellect = 10,
                }
            }
        };
        /// <summary>
        /// Stores all available Armor occupying the body slot.
        /// </summary>
        public List<Armor> bodyArmor = new()
        {
            new Armor()
            {
                EquipmentName = "Simple Cloth Armor",
                EquipmentLevel = 1,
                EquipmentSlot = Slot.SLOT_BODY,
                ArmorType = ArmorType.ARMOR_CLOTH,
                Stats = new()
                {
                    Strength = 1,
                    Dexterity = 1,
                    Intellect = 5,
                }
            },
            new Armor()
            {
                EquipmentName = "Advanced Cloth Armor",
                EquipmentLevel = 5,
                EquipmentSlot = Slot.SLOT_BODY,
                ArmorType = ArmorType.ARMOR_CLOTH,
                Stats = new()
                {
                    Strength = 10,
                    Dexterity = 10,
                    Intellect = 50,
                }
            },
            new Armor()
            {
                EquipmentName = "Simple Leather Armor",
                EquipmentLevel = 1,
                EquipmentSlot = Slot.SLOT_BODY,
                ArmorType = ArmorType.ARMOR_LEATHER,
                Stats = new()
                {
                    Strength = 1,
                    Dexterity = 5,
                    Intellect = 1,
                }
            },
            new Armor()
            {
                EquipmentName = "Advanced Leather Armor",
                EquipmentLevel = 5,
                EquipmentSlot = Slot.SLOT_BODY,
                ArmorType = ArmorType.ARMOR_LEATHER,
                Stats = new()
                {
                    Strength = 10,
                    Dexterity = 50,
                    Intellect = 10,
                }
            },
            new Armor()
            {
                EquipmentName = "Simple Mail Armor",
                EquipmentLevel = 1,
                EquipmentSlot = Slot.SLOT_BODY,
                ArmorType = ArmorType.ARMOR_MAIL,
                Stats = new()
                {
                    Strength = 2,
                    Dexterity = 4,
                    Intellect = 1,
                }
            },
            new Armor()
            {
                EquipmentName = "Advanced Mail Armor",
                EquipmentLevel = 5,
                EquipmentSlot = Slot.SLOT_BODY,
                ArmorType = ArmorType.ARMOR_MAIL,
                Stats = new()
                {
                    Strength = 20,
                    Dexterity = 40,
                    Intellect = 10,
                }
            },
            new Armor()
            {
                EquipmentName = "Simple Plate Armor",
                EquipmentLevel = 1,
                EquipmentSlot = Slot.SLOT_BODY,
                ArmorType = ArmorType.ARMOR_PLATE,
                Stats = new()
                {
                    Strength = 5,
                    Dexterity = 1,
                    Intellect = 1,
                }
            },
            new Armor()
            {
                EquipmentName = "Advanced Plate Armor",
                EquipmentLevel = 5,
                EquipmentSlot = Slot.SLOT_BODY,
                ArmorType = ArmorType.ARMOR_PLATE,
                Stats = new()
                {
                    Strength = 50,
                    Dexterity = 10,
                    Intellect = 10,
                }
            }
        };

        /// <summary>
        /// Stores all available Armor occupying the legs slot.
        /// </summary>
        public List<Armor> legArmor = new()
        {
            new Armor()
            {
                EquipmentName = "Simple Cloth Leggings",
                EquipmentLevel = 1,
                EquipmentSlot = Slot.SLOT_LEGS,
                ArmorType = ArmorType.ARMOR_CLOTH,
                Stats = new()
                {
                    Strength = 1,
                    Dexterity = 2,
                    Intellect = 4,
                }
            },
            new Armor()
            {
                EquipmentName = "Advanced Cloth Leggings",
                EquipmentLevel = 5,
                EquipmentSlot = Slot.SLOT_LEGS,
                ArmorType = ArmorType.ARMOR_CLOTH,
                Stats = new()
                {
                    Strength = 10,
                    Dexterity = 20,
                    Intellect = 40,
                }
            },
            new Armor()
            {
                EquipmentName = "Simple Leather Leggings",
                EquipmentLevel = 1,
                EquipmentSlot = Slot.SLOT_LEGS,
                ArmorType = ArmorType.ARMOR_LEATHER,
                Stats = new()
                {
                    Strength = 2,
                    Dexterity = 4,
                    Intellect = 2,
                }
            },
            new Armor()
            {
                EquipmentName = "Advanced Leather Leggings",
                EquipmentLevel = 5,
                EquipmentSlot = Slot.SLOT_LEGS,
                ArmorType = ArmorType.ARMOR_LEATHER,
                Stats = new()
                {
                    Strength = 20,
                    Dexterity = 40,
                    Intellect = 20,
                }
            },
            new Armor()
            {
                EquipmentName = "Simple Mail Leggings",
                EquipmentLevel = 1,
                EquipmentSlot = Slot.SLOT_LEGS,
                ArmorType = ArmorType.ARMOR_MAIL,
                Stats = new()
                {
                    Strength = 3,
                    Dexterity = 4,
                    Intellect = 1,
                }
            },
            new Armor()
            {
                EquipmentName = "Simple Mail Leggings",
                EquipmentLevel = 5,
                EquipmentSlot = Slot.SLOT_LEGS,
                ArmorType = ArmorType.ARMOR_MAIL,
                Stats = new()
                {
                    Strength = 30,
                    Dexterity = 40,
                    Intellect = 10,
                }
            },
            new Armor()
            {
                EquipmentName = "Simple Plate Leggings",
                EquipmentLevel = 1,
                EquipmentSlot = Slot.SLOT_LEGS,
                ArmorType = ArmorType.ARMOR_PLATE,
                Stats = new()
                {
                    Strength = 4,
                    Dexterity = 1,
                    Intellect = 2,
                }
            },
            new Armor()
            {
                EquipmentName = "Advanced Plate Leggings",
                EquipmentLevel = 5,
                EquipmentSlot = Slot.SLOT_LEGS,
                ArmorType = ArmorType.ARMOR_PLATE,
                Stats = new()
                {
                    Strength = 40,
                    Dexterity = 10,
                    Intellect = 20,
                }
            }
        };

        /// <summary>
        /// Stores all available Weapon occupying the weapon slot.
        /// </summary>
        public List<Weapon> weapons = new()
        {
            new Weapon()
            {
                EquipmentName = "Stone Axe",
                EquipmentLevel = 1,
                EquipmentSlot = Slot.SLOT_WEAPON,
                WeaponType = WeaponType.WEAPON_AXE,
                Stats = new()
                {
                    Damage = 15,
                    AttackSpeed = 0.75
                }
            },
            new Weapon()
            {
                EquipmentName = "Steel Axe",
                EquipmentLevel = 5,
                EquipmentSlot = Slot.SLOT_WEAPON,
                WeaponType = WeaponType.WEAPON_AXE,
                Stats = new()
                {
                    Damage = 150,
                    AttackSpeed = 0.75
                }
            },
            new Weapon()
            {
                EquipmentName = "Simple Bow",
                EquipmentLevel = 1,
                EquipmentSlot = Slot.SLOT_WEAPON,
                WeaponType = WeaponType.WEAPON_BOW,
                Stats = new()
                {
                    Damage = 7,
                    AttackSpeed = 1.2
                }
            },
            new Weapon()
            {
                EquipmentName = "Advanced Bow",
                EquipmentLevel = 5,
                EquipmentSlot = Slot.SLOT_WEAPON,
                WeaponType = WeaponType.WEAPON_BOW,
                Stats = new()
                {
                    Damage = 75,
                    AttackSpeed = 1.2
                }
            },
            new Weapon()
            {
                EquipmentName = "Rusted Dagger",
                EquipmentLevel = 1,
                EquipmentSlot = Slot.SLOT_WEAPON,
                WeaponType = WeaponType.WEAPON_DAGGER,
                Stats = new()
                {
                    Damage = 5,
                    AttackSpeed = 1.5
                }
            },
            new Weapon()
            {
                EquipmentName = "Steel Dagger",
                EquipmentLevel = 5,
                EquipmentSlot = Slot.SLOT_WEAPON,
                WeaponType = WeaponType.WEAPON_DAGGER,
                Stats = new()
                {
                    Damage = 50,
                    AttackSpeed = 1.5
                }
            },
            new Weapon()
            {
                EquipmentName = "Blunt Hammer",
                EquipmentLevel = 1,
                EquipmentSlot = Slot.SLOT_WEAPON,
                WeaponType = WeaponType.WEAPON_HAMMER,
                Stats = new()
                {
                    Damage = 20,
                    AttackSpeed = 0.67
                }
            },
            new Weapon()
            {
                EquipmentName = "Spiked Hammer",
                EquipmentLevel = 5,
                EquipmentSlot = Slot.SLOT_WEAPON,
                WeaponType = WeaponType.WEAPON_HAMMER,
                Stats = new()
                {
                    Damage = 200,
                    AttackSpeed = 0.67
                }
            },
            new Weapon()
            {
                EquipmentName = "Simple Staff",
                EquipmentLevel = 1,
                EquipmentSlot = Slot.SLOT_WEAPON,
                WeaponType = WeaponType.WEAPON_STAFF,
                Stats = new()
                {
                    Damage = 2,
                    AttackSpeed = 1.75
                }
            },
            new Weapon()
            {
                EquipmentName = "Arcane Staff",
                EquipmentLevel = 5,
                EquipmentSlot = Slot.SLOT_WEAPON,
                WeaponType = WeaponType.WEAPON_STAFF,
                Stats = new()
                {
                    Damage = 25,
                    AttackSpeed = 1.75
                }
            },
            new Weapon()
            {
                EquipmentName = "Basic Sword",
                EquipmentLevel = 1,
                EquipmentSlot = Slot.SLOT_WEAPON,
                WeaponType = WeaponType.WEAPON_SWORD,
                Stats = new()
                {
                    Damage = 10,
                    AttackSpeed = 1
                }
            },
            new Weapon()
            {
                EquipmentName = "Advanced Sword",
                EquipmentLevel = 5,
                EquipmentSlot = Slot.SLOT_WEAPON,
                WeaponType = WeaponType.WEAPON_SWORD,
                Stats = new()
                {
                    Damage = 100,
                    AttackSpeed = 1
                }
            },
            new Weapon()
            {
                EquipmentName = "Simple Wand",
                EquipmentLevel = 1,
                EquipmentSlot = Slot.SLOT_WEAPON,
                WeaponType = WeaponType.WEAPON_WAND,
                Stats = new()
                {
                    Damage = 2,
                    AttackSpeed = 2
                }
            },
            new Weapon()
            {
                EquipmentName = "Arcane Wand",
                EquipmentLevel = 5,
                EquipmentSlot = Slot.SLOT_WEAPON,
                WeaponType = WeaponType.WEAPON_WAND,
                Stats = new()
                {
                    Damage = 20,
                    AttackSpeed = 2
                }
            }
        };
        public ItemCollections() { }
    }
}
