﻿namespace Assignment04.Collections
{
    /// <summary>
    /// Enum that specifies all armor types as type ArmorType.
    /// </summary>
    public enum ArmorType
    {
        ARMOR_CLOTH,
        ARMOR_LEATHER,
        ARMOR_MAIL,
        ARMOR_PLATE
    }
}
