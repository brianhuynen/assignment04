﻿namespace Assignment04.Collections
{
    /// <summary>
    /// Enum that specifies all weapon types as type WeaponType.
    /// </summary>
    public enum WeaponType
    {
        WEAPON_AXE,
        WEAPON_BOW,
        WEAPON_DAGGER,
        WEAPON_HAMMER,
        WEAPON_STAFF,
        WEAPON_SWORD,
        WEAPON_WAND
    }
}
