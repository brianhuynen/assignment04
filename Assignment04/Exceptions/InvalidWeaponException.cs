﻿namespace Assignment04.Exceptions
{
    /// <summary>
    /// Custom exception that is thrown during errors involving operations with Weapon objects.
    /// </summary>
    public class InvalidWeaponException : Exception
    {
        public InvalidWeaponException() { }

        public InvalidWeaponException(string message) : base(message) { }

        public InvalidWeaponException(string message, Exception innerException) : base(message, innerException) { }
    }
}
