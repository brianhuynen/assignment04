﻿namespace Assignment04.Exceptions
{
    /// <summary>
    /// Custom exception that is thrown during errors involving operations with Armor objects.
    /// </summary>
    public class InvalidArmorException : Exception
    {
        public InvalidArmorException() { }

        public InvalidArmorException(string message) : base(message) { }

        public InvalidArmorException(string message, Exception innerException) : base(message, innerException) { }
    }
}
