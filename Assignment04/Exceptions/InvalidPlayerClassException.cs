﻿namespace Assignment04.Exceptions
{
    /// <summary>
    /// Custom exception that is thrown during errors involving operations with Player classes.
    /// </summary>
    public class InvalidPlayerClassException : Exception
    {
        public InvalidPlayerClassException() { }
        public InvalidPlayerClassException(string message) : base(message) { }
        public InvalidPlayerClassException(string message, Exception innerException) : base(message, innerException) { }
    }
}
