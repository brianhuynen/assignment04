﻿namespace Assignment04.Attributes
{
    /// <summary>
    /// Class that represents the weapon attributes Damage and AttackSpeed.
    /// </summary>
    public class WeaponAttribute
    {
        public int Damage { get; set; }
        public double AttackSpeed { get; set; }
        public WeaponAttribute() { }
        /// <summary>
        /// Views the data of this WeaponAttribute.
        /// </summary>
        /// <returns>A string containing all data: Damage and AttackSpeed.</returns>
        public string View()
        {
            return
                $"Damage    : {this.Damage}\n" +
                $"AttackSpd : {this.AttackSpeed}";
        }
    }
}
