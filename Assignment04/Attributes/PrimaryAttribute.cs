﻿namespace Assignment04.Attributes
{
    /// <summary>
    /// Class that represents the primary attributes Strength, Dexterity and Intellect.
    /// </summary>
    public class PrimaryAttribute
    {
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intellect { get; set; }
        public PrimaryAttribute() { }
        /// <summary>
        /// Views the data of this PrimaryAttribute.
        /// </summary>
        /// <returns>A string containing all data: Strength, Dexterity and Intellect.</returns>
        public string View()
        {
            return
                $"Strength  : {this.Strength}\n" +
                $"Dexterity : {this.Dexterity}\n" +
                $"Intellect : {this.Intellect}";
        }
    }
}
