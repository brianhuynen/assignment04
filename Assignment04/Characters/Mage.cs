﻿using Assignment04.Attributes;
using Assignment04.Collections;
using Assignment04.Items;
using Assignment04.Items.Armor;
using Assignment04.Items.Weapon;

namespace Assignment04.Characters
{
    /// <summary>
    /// Class to represent the Player subclass Mage.
    /// </summary>
    public class Mage : Player
    {
        public override void InitStats()
        {
            this.Stats = new PrimaryAttribute()
            { Strength = 1, Dexterity = 1, Intellect = 8 };
            this.LevelupStats = new PrimaryAttribute()
            { Strength = 1, Dexterity = 1, Intellect = 5 };
            this.TotalStats = this.Stats;
        }
        public override string CanEquip(Equipment equip)
        {
            if (equip.GetType() == typeof(Armor))
            {
                Armor armor = (Armor)equip;
                if (armor.ArmorType != ArmorType.ARMOR_CLOTH)
                    return $"Cannot equip armor type {armor.ArmorType} to {this.PlayerClass} class.";
            }

            if (equip.GetType() == typeof(Weapon))
            {
                Weapon weapon = (Weapon)equip;
                if (weapon.WeaponType != WeaponType.WEAPON_STAFF && weapon.WeaponType != WeaponType.WEAPON_WAND)
                    return $"Cannot weapon armor type {weapon.WeaponType} to {this.PlayerClass} class.";
            }

            if (this.PlayerLevel < equip.EquipmentLevel)
                return $"Weapon level is too high (requires lv. {equip.EquipmentLevel}, player is lv. {this.PlayerLevel})";

            return "";
        }
    }
}
