﻿using Assignment04.Attributes;
using Assignment04.Collections;
using Assignment04.Items;
using Assignment04.Items.Armor;
using Assignment04.Items.Weapon;

namespace Assignment04.Characters
{
    /// <summary>
    /// Class to represent the Player subclass Ranger.
    /// </summary>
    public class Ranger : Player
    {
        public override void InitStats()
        {
            this.Stats = new PrimaryAttribute()
            { Strength = 1, Dexterity = 7, Intellect = 1 };
            this.LevelupStats = new PrimaryAttribute()
            { Strength = 1, Dexterity = 5, Intellect = 1 };
            this.TotalStats = this.Stats;
        }
        public override string CanEquip(Equipment equip)
        {
            bool typeMatch = true;
            if (equip.GetType() == typeof(Armor))
            {
                Armor armor = (Armor)equip;
                if (armor.ArmorType != ArmorType.ARMOR_LEATHER && armor.ArmorType != ArmorType.ARMOR_MAIL)
                    return $"Cannot equip armor type {armor.ArmorType} to {this.PlayerClass} class.";
            }
            
            if (equip.GetType() == typeof(Weapon))
            {
                Weapon weapon = (Weapon)equip;
                if (weapon.WeaponType != WeaponType.WEAPON_BOW)
                    return $"Cannot weapon armor type {weapon.WeaponType} to {this.PlayerClass} class.";
            }

            if (this.PlayerLevel < equip.EquipmentLevel)
                return $"Weapon level is too high (requires lv. {equip.EquipmentLevel}, player is lv. {this.PlayerLevel})";

            return "";
        }
    }
}
