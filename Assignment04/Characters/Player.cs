﻿using Assignment04.Items;
using Assignment04.Items.Armor;
using Assignment04.Items.Weapon;
using Assignment04.Attributes;
using Assignment04.Exceptions;

namespace Assignment04.Characters
{
    public abstract class Player
    {
        // Equipment related fields
        Dictionary<Slot, Equipment> equipment = new();
        public Dictionary<Slot, Equipment> Equipment { get => equipment; }

        // Player-specific fields
        public string PlayerName { get; set; }
        public int PlayerLevel { get; set; }
        public string PlayerClass { get; set; }

        // Stats-related fields
        public PrimaryAttribute Stats { get; set; }
        public PrimaryAttribute LevelupStats { get; set; }
        public PrimaryAttribute ArmorStats { get; set; } = new() { Strength = 0, Dexterity = 0, Intellect = 0 };
        public PrimaryAttribute TotalStats { get; set; } = new() { Strength = 0, Dexterity = 0, Intellect = 0 };

        /// <summary>
        /// Constructor calls the InitStats() method which initializes the class-specific stats for each class.
        /// These are specified in the Mage, Ranger, Rogue and Warrior class files.
        /// It is handled in the main class that playername/ playerclass cannot be null.
        /// </summary>
        public Player() 
        {
            this.PlayerLevel = 1;
            InitStats();
        }
        /// <summary>
        /// Initializes the stats of a Player upon construction. This varies per Player subclass.
        /// </summary>
        public abstract void InitStats();
        /// <summary>
        /// Handles whether a player can equip a certain item.
        /// </summary>
        /// <param name="equip">The equipment to be equipped.</param>
        /// <returns>A string value representing the result. Either it contains an empty string or a message that can be used as exception message.</returns>
        public abstract string CanEquip(Equipment equip);
        /// <summary>
        /// Calculates the total damage the player can deal based on their attributes and equipment.
        /// </summary>
        /// <returns>A double representing the total damage a player can deal.</returns>
        /// <exception cref="InvalidPlayerClassException">Should the Player be of a class that does not exist/ the string PlayerClass was specified incorrectly, it returns an InvalidPlayerClassException.</exception>
        public double TotalDamage()
        {
            //Initialize dps. It is initialized as 1 since the DPS of a player without a weapon equipped is specified as 1
            double dps = 1;

            //Assign proper DPS if the player has a weapon equipped
            if (this.Equipment.ContainsKey(Slot.SLOT_WEAPON))
            {
                Weapon weapon = (Weapon)this.Equipment[Slot.SLOT_WEAPON];
                dps = weapon.DPS();
            }
            //Ensures the right calculations are made
            switch(this.PlayerClass)
            {
                case "Mage":
                    return dps * (1 + ((double)this.TotalStats.Intellect / 100));
                case "Ranger":
                    return dps * (1 + ((double)this.TotalStats.Dexterity / 100));
                case "Rogue":
                    return dps * (1 + ((double)this.TotalStats.Dexterity / 100));
                case "Warrior":
                    return dps * (1 + ((double)this.TotalStats.Strength / 100));
                default:
                    throw new InvalidPlayerClassException($"This class does not exist (Classname = {this.PlayerClass}).");
            }
        }
        /// <summary>
        /// Handles leveling up a player
        /// </summary>
        /// <returns>A string that notifies the console of a player leveling up, as well as its stat changes.</returns>
        public string LevelUp()
        {
            int oldLevel = this.PlayerLevel,
                oldStrength = this.Stats.Strength,
                oldDexterity = this.Stats.Dexterity,
                oldIntellect = this.Stats.Intellect;

            this.PlayerLevel++;

            this.Stats = new()
            {
                Strength = this.Stats.Strength += this.LevelupStats.Strength,
                Dexterity = this.Stats.Dexterity += this.LevelupStats.Dexterity,
                Intellect = this.Stats.Intellect += this.LevelupStats.Intellect
            };

            UpdateStats();

            return 
                $"{this.PlayerName} leveled up!\n" +
                $"Level     : {oldLevel} -> {this.PlayerLevel}\n" +
                $"Strength  : {oldStrength} -> {this.Stats.Strength} (+{this.LevelupStats.Strength})\n" +
                $"Dexterity : {oldDexterity} -> {this.Stats.Dexterity} (+{this.LevelupStats.Dexterity})\n" +
                $"Intellect : {oldIntellect} -> {this.Stats.Intellect} (+{this.LevelupStats.Intellect})\n";
        }
        /// <summary>
        /// Prints the player data to the console.
        /// </summary>
        /// <returns>A string representing the player data.</returns>
        public string ViewPlayer()
        {
            return 
                $"Playername: {this.PlayerName} the {this.PlayerClass}\n" +
                $"Level     : {this.PlayerLevel}\n" +
                $"\n" +
                $"Stats     :\n" +
                $"Strength  : {this.Stats.Strength} (+{this.ArmorStats.Strength}) : {this.TotalStats.Strength}\n" +
                $"Dexterity : {this.Stats.Dexterity} (+{this.ArmorStats.Dexterity}) : {this.TotalStats.Dexterity}\n" +
                $"Intellect : {this.Stats.Intellect} (+{this.ArmorStats.Intellect}) : {this.TotalStats.Intellect}\n" +
                $"\n" +
                $"{(this.Equipment.ContainsKey(Slot.SLOT_WEAPON) ? this.Equipment[Slot.SLOT_WEAPON].EquipmentStats() : "Damage    : 1\nAttackSpd : 1\n")}" +
                $"TotalDPS  : {this.TotalDamage()}";
        }
        /// <summary>
        /// Prints the equipment a player has equipped to the console.
        /// </summary>
        /// <returns>A string representing the equipped equipment.</returns>
        public string ViewEquipment()
        {
            string equippedItems = "";
            //If nothing is equipped, view "Nothing" rather than an empty string.
            if (equipment.Count == 0)
                equippedItems = "Nothing";
            else
            {
                foreach (KeyValuePair<Slot, Equipment> eqp in equipment)
                {
                    if (eqp.Value.GetType() == typeof(Armor))
                    {
                        Armor armor = (Armor)eqp.Value;
                        equippedItems += armor.EquipmentData() + "\n";
                    }
                    if (eqp.Value.GetType() == typeof(Weapon))
                    {
                        Weapon weapon = (Weapon)eqp.Value;
                        equippedItems += weapon.EquipmentData() + "\n";
                    }
                }
            }
            return
            $"Equipped  :\n" +
            $"{equippedItems}";
        }
        /// <summary>
        /// Is called in any function that changes the stats of a player. Handles the correct total stats are stored.
        /// </summary>
        public void UpdateStats()
        {
            PrimaryAttribute tempAttribute = new() { Strength = 0, Dexterity = 0, Intellect = 0 };
            //Checks the equipment first and adds the stats of each item to a temporary PrimaryAttribute.
            foreach (KeyValuePair<Slot, Equipment> eqp in equipment)
            {
                if (eqp.Value.GetType() == typeof(Armor))
                {
                    Armor armor = (Armor)eqp.Value;
                    tempAttribute.Strength += armor.Stats.Strength;
                    tempAttribute.Dexterity += armor.Stats.Dexterity;
                    tempAttribute.Intellect += armor.Stats.Intellect;
                }
            }

            this.ArmorStats = tempAttribute;
            //Updates the total stats.
            this.TotalStats = new()
            {
                Strength = this.Stats.Strength + this.ArmorStats.Strength,
                Dexterity = this.Stats.Dexterity + this.ArmorStats.Dexterity,
                Intellect = this.Stats.Intellect + this.ArmorStats.Intellect
            };
        }
        /// <summary>
        /// Handles equipping an equipment to the player.
        /// </summary>
        /// <param name="eqp">The equipment that is being added to the player.</param>
        public void Equip(Equipment eqp)
        {
            Console.WriteLine($"Equipping {eqp.EquipmentName}...");
            equipment.Add(eqp.EquipmentSlot, eqp);
            Console.WriteLine($"{eqp.EquipmentName} was equipped!");
            UpdateStats();
        }
        /// <summary>
        /// Handles unequipping an equipment from the player.
        /// </summary>
        /// <param name="slot">The itemslot from which the equipment has to be removed.</param>
        public void Unequip(Slot slot)
        {
            Console.WriteLine($"Equipment {equipment[slot].EquipmentName} already exists in given slot. Unequipping...");
            equipment.Remove(slot);
            Console.WriteLine("Unequipped succesfully!");
            UpdateStats();
        }
    }
}
